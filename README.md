# Usage

## Development 

Pre-requests: 

* python 3rd version (tested on 3.7)
* docker (tested on version 18.09.2, build 6247962)
* docker-compose (tested on version 1.23.2, build 1110ad01)

Run development environment: 

    docker-compose up 
    
Rebuild project

    docker-compose up --build

## Description 

This project is configured to be used with GitLab CI/CD and kubernetes cluster in AWS. 

Docker images storage is AWS ECR.

Ansible was chosen as IaC tool. Ansible wraps CloudFormation config files and `kubectl` usage. 

GitLab runner for CI/CD runs as a standalone AWS EC2 instance and provisioned with Ansible. 

To use Ansible `cd ./infra`, install all python dependencies `pip install -r requirements.pip`

## CI/CD overview

![CI/CD pipeline](docs/images/dia.png)

## Git strategy and CI/CD pipeline

pre branch strategy:

* **master** - mainstream branch, every commit goes and runs tests. 
    if test succeeds staging pipeline stage starts
* **staging** - If master test passes new version tag issues and merges to staging branch. 
    builds new images, with version and commit hash tags. And uploads it to AWS ECR 
    than staging k8s cluster updates images with rolling update
* **production** - Production pipeline stage initiates manually.
    merges with staging
    rolls updates to production k8s cluster

![CI/CD pipeline](docs/images/gitlab-pipeline.png)

## Version strategy 

`.version` file content is actual version, content will be used to tag docker image 
    
# Provisioning notes 

* AWSCLI must be ![installed](https://docs.aws.amazon.com/en_us/cli/latest/userguide/cli-chap-install.html) and inited inited (KEY, SECRET and REGION) `awscli configure`
* Kubctl must be ![installed and inited](https://docs.aws.amazon.com/en_us/eks/latest/userguide/getting-started.html) 
* SSH pub key must be presented in `~/.ssh/id_rsa.pub`

# Manual operation notes 

1. Gitlab CI/CD have to be configured manual, it still does not have API 
You will have to add AWS credential for storing images into ECR: 

![env setup](docs/images/gitlab-settings-cicd-env.png)

2. GitLab K8S integration 

![env setup](docs/images/gitlab-k8s-setup.png)


# App notes
 
This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.


