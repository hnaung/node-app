//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();


chai.use(chaiHttp);


describe('API Server', () => {
    describe('/GET status', () => {
        it('Should get status', (done) => {
            chai.request(server)
                .get('/api/status')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('request_uuid');
                    res.body.should.have.property('time');
                    done();
                });
        });
    });
});